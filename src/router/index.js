import Vue from 'vue'
import VueRouter from 'vue-router'
// import Home from '../views/Home.vue'
import Login from '../components/login/login.vue'
import Register from '../components/register/register.vue'
import Forgot from '../components/forgot/forgot.vue'
import Test from '../components/test/test.vue'
import Manager from '../components/manager/manager.vue'
import Host from '../components/host/host.vue'
import Waiter from '../components/waiter/waiter.vue'
import Busboy from '../components/busboy/busboy.vue'
import Cook from '../components/cook/cook.vue'
import About from '../components/about/about.vue'
import WaiterO from '../components/waiter/order_waiter.vue'
import Food from '../components/waiter/food.vue'
import StapleFood from '../components/waiter/staple_food.vue'
import Drink from '../components/waiter/drink.vue'
import Soup from '../components/waiter/soup.vue'
import Haha from '../components/waiter/haha.vue'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Login',
    component: Login,
    meta: {
      title: '登录'
    }
  },
  {
    path: '/register',
    name: 'Register',
    component: Register,
    meta: {
      title: '注册'
    }
  },
  {
    path: '/forgot',
    name: 'Forgot',
    component: Forgot,
    meta: {
      title: '找回密码'
    }
  },
  {
    path: '/test',
    name: 'Test',
    component: Test
  },
  {
    path: '/manager',
    name: 'Manager',
    component: Manager,
    meta: {
      title: 'manager'
    }
  },
  {
    path: '/waiter',
    name: 'Waiter',
    component: Waiter,
    meta: {
      title: 'waiter'
    }
  },
  {
    path: '/host',
    name: 'Host',
    component: Host,
    meta: {
      title: 'host'
    }
  },
  {
    path: '/busboy',
    name: 'Busboy',
    component: Busboy,
    meta: {
      title: 'busboy'
    }
  },
  {
    path: '/cook',
    name: 'Cook',
    component: Cook,
    meta: {
      title: 'cook'
    }
  },
  {
    path: '/about',
    name: 'About',
    component: About,
    meta: {
      title: 'about'
    }
  },
  {
    path: '/haha',
    name: 'Haha',
    component: Haha
  },
  {
    path: '/waiter/order',
    name: 'WaiterO',
    component: WaiterO
  },
  {
    path: '/order',
    name: 'WaiterO',
    component: WaiterO,
    children: [
      {
        path: 'food',
        name: 'food',
        component: Food
      },
      {
        path: 'drink',
        name: 'drink',
        component: Drink
      },
      {
        path: 'soup',
        name: 'soup',
        component: Soup
      },
      {
        path: 'staple_food',
        name: 'staple_food',
        component: StapleFood
      }
    ]
  }
]

const router = new VueRouter({
  routes
})

export default router
